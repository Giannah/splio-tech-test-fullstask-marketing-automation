import org.junit.Assert;
import org.junit.Test;

public class MarathonTest {
    @Test
    public void Test() {
        Marathon marathon = new Marathon();
        String result = marathon.calculateResult();
        Assert.assertEquals("100", result);
    }
}
