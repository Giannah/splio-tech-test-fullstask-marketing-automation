# Splio - Tech Test Fullstack Marketing Automation

## Back-End

This is a Maven project. 

If you're using JetBrain's IntelliJ IDEA, clone this repo and open it in your IDE and everything should go smoothly.

If you're using Microsoft's VSCode, please refer to [the doc](https://code.visualstudio.com/docs/languages/java).

If using any other IDE, refer to the doc for specs.